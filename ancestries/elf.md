---
layout: default
sidebarname: Elf
sidebartype: ancestries
---

# Elf

<p class="flavour">
Elves are stuck up tree huggers.
</p>

<p>
In addition to the features listed in the appropriate section, all elves have the following:
</p>

{% include action.liquid action=site.data.actions.trance %}
{% include action.liquid action=site.data.actions.darkvision %}

## High Elf
{% include ancestrybox.liquid ancestry=site.data.ancestries.elf_high %}

<p class="flavour">
High elves are very good at magic.
</p>
