---
layout: default
sidebarname: Common
sidebartype: actions
sidebarsort: 1
---

# Common Actions

Common actions are available to all creatures, regardless of choices chosen in character selection.

### Full List

{% include actionlist.liquid type="common" %}
