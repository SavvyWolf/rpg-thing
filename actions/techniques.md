---
layout: default
sidebarname: Techniques
sidebartype: actions
sidebarsort: 2
has_children: techniques
---

# Techniques

Techniques are special actions that creatures can employ in battle. Each class teaches a hero a specific amount of
techniques, which increases as they increase in level.

Typically, the number of techniques known is dependant on an ability score. When calculating the amount of techniques
known, only permanent sources of ability score increases should be considered.

Any techniques that are given directly (such as through leveling, ancestries or other sources) do not count towards
this limit. Creatures are trained in specific disciplines, and when choosing techniques may only pick those that have at
least one of those disciplines listed.

## Disciplines
The following are all the current disciplines for techniques:
* [Martial]({{site.baseurl}}/actions/techniques/martial.html)

## Full List
{% include actionlist.liquid type="technique" %}
