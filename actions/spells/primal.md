---
layout: default
sidebarname: Primal
parent: spells
---

# Primal Spells

{% include actionlist.liquid discipline="primal" %}
