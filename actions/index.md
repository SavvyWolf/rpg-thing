---
layout: default
sidebarname: Overview
sidebartype: actions
sidebarsort: 0
---

# Feats

## Timing Windows
In terms of timing, actions are categorized into the following timing windows:
<ul>
<li>{% include window.liquid window="extended" %} Extended</li>
<li>{% include window.liquid window="major" %} Major</li>
<li>{% include window.liquid window="minor" %} Minor</li>
<li>{% include window.liquid window="move" %} Move</li>
<li>{% include window.liquid window="reaction" %} Reaction</li>
<li>{% include window.liquid window="free" %} Free</li>
<li>{% include window.liquid window="passive" %} Passive</li>
<li>{% include window.liquid window="boon" %} Boon</li>
</ul>

Extended actions have a duration longer than 6 seconds, which is listed in their description. Reactions have a trigger
which describes when they can be used (and they may be used instantly at that point). Free and passive actions don't
take any time to use.

## Reading An Feat Entry
Feat entries have the following information displayed with them, as follows.

{% include action.liquid action=site.data.actions.shove %}

### Timing Window
The timing window that this action requires, as an icon. Hover your mouse over the icon to see the specific timing
window. In order to use an action with Major, Minor, Move or Reaction timing windows, you must have such a window
available, and using the action consumes it.

Outside of combat, timing windows other than extended do not matter.

### Tags
Displayed in italics under the action name is a list of tags. The type of action is always the first tag (and is also
communicated with the colour of the box itself). Any following entries are additional tags. These extra tags do not have
meanings in the core rules themselves, but other abilities may refer to them.

### Minimum Level
The level requirement to use this ability. If you are lower than the action's required level, then you cannot use the
action. If this field is absent, then there is no level requirement for the ability.

### Disciplines
The disciplines that this technique or spell belongs to. Heroes may only chose abilities in disciplines that they have
access to. If this field isn't present, then the action cannot be chosen as part of a hero's chosen techniques or spells
(but may be given as part of leveling up in a class, or through other means).

### Cooldown
When this action is used, it cannot be used again until after the start of the creature's turn in a specific number of
rounds. For example, when a technique with a cooldown of 2 is used, it cannot be used the following turn, but can be
used the turn after that.

To track this, consider placing a dice on a character sheet after using an ability with the face showing the cooldown.
At the end of the creature's turn (including the one it used the ability), reduce the number on the dice by one,
removing it when it hits zero.

If this field is absent, then there is no limit to how frequently the creature can use the ability, provided they have
the resources.

### Duration
This is the time the action takes.

For Extended Actions, this is the time the character must focus on performing the action; they must spend a major
action every turn during combat focusing on the ability. Outside of combat, they may do other things as long they
continue performing the action.

For other actions, this is the duration of the effect of the action. After the action has been performed, the effect
given in its description will linger for the specified amount of time. The creature performing the action does not need
to perform any further actions to preserve the effect.

### Trigger
For reactions, this details the conditions under which a creature may take the reaction. The ability may only be used
in those cases, and that is the point where the player decides whether or not to use their reaction.

### Untrained Penalty
This action can be used even when a creature hasn't learned the technique, spell or trick. However, they take the given
penalty dice on all rolls made as part of that ability.

If this field isn't present, then the creature cannot use the ability without learning it first.

### Critical Effects
If any roll while using the ability is a critical, then the listed effects happen as follows:

* Automatically Succeeds: The check will automatically succeed.
* Double Damage: Double the amount of dice rolled when dealing damage. For example, an action that deals 2d10 + STR
  damage would instead deal 4d10 + STR.

### Description
The main text of the ability describes its effects.

## Feat Tags
