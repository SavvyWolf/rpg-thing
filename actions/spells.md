---
layout: default
sidebarname: Spells
sidebartype: actions
sidebarsort: 2
has_children: spells
---

# Spells

Spells are magical abilities that creatures can use if they have specific classes. Each class teaches a hero a specific
amount of spells, which increases as they increase in level.

Typically, the number of spells known is dependant on an ability score. When calculating the amount of spells known,
only permanent sources of ability score increases should be considered.

Any spells that are given directly (such as through leveling, ancestries or other sources) do not count towards this
limit. Creatures are trained in specific disciplines, and when choosing spells may only pick those that have at
least one of those disciplines listed.

## Disciplines
The following are all the current disciplines for spells:
* [Arcane]({{site.baseurl}}/actions/spells/arcane.html)

## Full List
{% include actionlist.liquid type="spell" %}
