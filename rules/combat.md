---
layout: default
sidebarname: Combat
sidebartype: rules
sidebarsort: 2
---

# Combat Rules

## Structure of Combat
Combat is divided into "rounds" that happen repeatedly until combat ends. In each round, each creature takes one turn.
On its turn, each creature can spend a move action, a major action and a minor action. At the start of its turn, it also
gains a reaction to use at any point in the current or any future turn.

Creatures may also use any number of free actions a turn, and can continue performing an extended action at a rate of
6 seconds of progress a turn.

A round lasts roughly 6 seconds, and turns occur mostly simultaneously during this time. The turn order represents each
creature's ability to start reacting sooner during a round.

## Initiative
### Starting Combat
When combat breaks out between two or more creatures (as determined by the GM), they all make opposed `1d20 + DEX`
checks, which determines the turn order. Unaware creatures don't make this check, and do not take actions in combat
until aware of it (either by being attacked, hearing commotion, and so on).

### Adding Creatures to the Initiative
A creature that enters combat after it has begun has to wait until the next round. At the start of the round (before
the creature that would act first's turn), they roll initiative as above, and get inserted into the appropriate position
as if they were in combat at the start.

This means that if a creature is ambushed, the attackers will usually get a full round of a attacks before the target
can react.
