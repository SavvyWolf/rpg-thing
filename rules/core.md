---
layout: default
sidebarname: Core Rules
sidebartype: rules
sidebarsort: 1
---

# Core Rules

This page details the main fundamental rules of (TODO: System name), and should be used as a quick reference for how to
play.

## Creatures and Heroes
A creature is a being in the world that can act upon its environment. This is a general term used to refer to animals,
NPCs, undead and even sentient items. Every creature has the following attributes (detailed in the "attributes"
section):

* Current, temporary and max hit points.
* The six ability scores.
* The five defences.
* A specific build (Small, Medium, Large, etc.).
* Access to common feats.

Depending on the specific creature, they may also have the following:

* Spell slots.
* Proficiencies.
* Additional feats (Abilities, Tricks, Techniques, Spells or Boosts).
* Class levels.

A "Hero" is a creature that has class levels and is controlled by a player, regardless of their moral inclinations.

## Resources
### HP and Dying
A creature begins each day after a long rest with hit points equal to their maximum and can lose them through taking
damage. If a creature runs out of hit points, they fall Unconscious and start Dying.

> TODO: Dying rules

### Spell Slots
A creature begins each day with a number of spell slots available depending on their class and level (see the
[appropriate table]({% link references/spellslots.md %})). These spell slots can be used once each to cast a spell of
their respective level or lower.

### Supplies
Supplies are all the essentials such as food and water that are required for survival in a hostile environment.

Medium creatures can carry up to 10 times their strength score (not their bonus), and must consume 10 supplies a day to
remain healthy. Due to their smaller stature, small creatures only need 8 supplies per day, but can only carry 8 times
their strength score in supplies.

Supplies are tracked separately from normal inventory items, and do not contribute to the normal carrying capacity
limit.

## Taking Actions
A creature can learn feats, typically from their ancestry, background and class, which give them actions that they can
do in and out of combat. All creatures also have access to Common feats and feats with an untrained penalty. Any feat
that isn't {% include window.liquid window="passive" %} Passive or a {% include window.liquid window="boon" %} Boon,
have associated actions.

Feats may be performed at any time out of combat, however, during combat, creatures may only use one
{% include window.liquid window="major" %} Major Action, one {% include window.liquid window="minor" %} Minor Action and
one {% include window.liquid window="reaction" %} Reaction per round. Regardless of when an action is used, it may have
pre-requisites that need to be met:

* If the action is a spell, a spell slot of the appropriate level or higher must be spent.
* Some skills have a cooldown, where they can't be used for that many combat rounds.

To perform an action, follow the instructions for that action in order, making any decisions as you encounter them.

Of course, you may freely attempt to do anything that doesn't have an appropriate feat. In which case, the GM will
adjudicate and decide what steps and rolls need to be made.

## Standard Attacks
Many actions will call for a standard attack, which will be described as the following:

> Make a [proficiency] [stat] attack vs [check]

To do this, you make the following roll:

> `1d20 + [stat] + BLD vs [check]`

For example, a regular `Strike (Melee)` calls for a "Weapon STR attack vs AD", so for a medium creature with a +3 bonus
to strength attacking a creature with an AD of 10, this would be `1d20 + 3 + 0 vs 10`.

A proficiency with a weapon or skill may be required. If a "Weapon" proficiency is required, but you lack the specific
proficiency, you take a `d6` penalty dice on the roll. Likewise, if a skill proficiency is required and you lack that
proficiency, you take a `d6` penalty dice on the roll.

## Standard Checks
Standard checks are similar to standard attacks, but do not include the creature's `BLD` and typically have a fixed
check value rather than a creature's defence.

They are made with the following roll:

> `1d20 + [stat] vs [check]`

Similar to standard attacks, proficiency with a weapon or skill proficiency may be required. Lacking the proficiency
imposes a `d6` penalty dice.

## Common Abbreviations
### Ability Scores

| Abbreviation | Meaning                         |
|--------------|---------------------------------|
| STR          | A creature's Strength bonus     |
| DEX          | A creature's Dexterity bonus    |
| CON          | A creature's Constitution bonus |
| INT          | A creature's Intelligence bonus |
| WIS          | A creature's Wisdom bonus       |
| CHA          | A creature's Charisma bonus     |

### Defences

| Abbreviation | Meaning                         |
|--------------|---------------------------------|
| AD           | A creature's Armour Defence     |
| RD           | A creature's Reflex Defence     |
| FD           | A creature's Fortitude Defence  |
| ED           | A creature's Endurance Defence  |
| WD           | A creature's Will Defence       |

### Other Attributes

| Abbreviation | Meaning                                                         |
|--------------|-----------------------------------------------------------------|
| HP           | A creature's Hit Points                                         |
| BLD          | A creature's Build bonus (0 for small and medium creatures)     |
| SAM          | Spellcasting Ability Modifier, class dependent                  |
| Rank         | The level of spell slot used to cast a spell                    |