---
layout: default
sidebarname: Spellcasting
sidebartype: rules
sidebarsort: 2
---

# Spellcasting

## Casting a Spell
Casting a spell is performed like any other action, except that they also require a spell slot to be expended.

Creatures have access to spell slots which allow them to cast spells. In order to cast a spell, a creature must spend a
"slot" of a specific rank. Spell slots are recovered at the end of a long rest. All creatures have infinite spell slots
for 0th rank spells, meaning they can be cast freely.

Spells can be cast with a higher rank spell slot than their minimum. Some spells mention the spell's rank in their
description, and so casting a spell in this way allows them to use a more powerful version of that spell.

## Spells Known
A creature's spell list contains spells that they have chosen during character creation or as part of leveling up, and
spells that they gain through other means. The amount of spells a creature can choose in this way is shown in their
class table under "spells".

By default, once a spell has been chosen, it can't be changed. However, many classes do have the ability to allow
creatures to change their list of chosen spells, typically after a long rest. Creatures cannot swap out spells that they
got through other means.

## Spellcasting Ability Modifier
Some spell descriptions mention `SAM`, which is a creature's Spellcasting Ability Modifier. By default, this is the
creature's `INT`, `WIS` or `CHA` bonus (its choice, typically whichever is the highest). However, many classes that
give access to spellcasting will require a specific modifier to be used (for example, Wizards must use their `INT`
modifier in order to access their full selection of spell slots).

## Concentration
Some spells have a duration listed as "concentration". A creature can hold concentration on one spell at a time (if it
would start concentrating on another, it breaks concentration on either of the two spells). When concentration is broken
on a spell, it's duration ends instantly.

The following situations break concentration on a spell:
* The caster receiving a critical hit from an ability that dealt damage.
* The caster falling unconscious.
