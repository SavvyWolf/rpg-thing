---
layout: default
sidebarname: Overview
sidebartype: rules
sidebarsort: 0
---

# Rules Overview

## Applying Rules
The GM has the final say in how rules are interpreted and what implications they have.

As a general rule, however, where two or more rules contradict each other, the more specific rule takes priority.

## Optional Rules
<div class="optional">
Sections like this denote optional rules or variants on rules. The GM may or may not decide to use them.
</div>

## Rolling Dice
This game requires the following dice: d4, d6, d8, d10, d12, d20 and a d100. The notation used to roll dice is
`xdn + y`. For example, `1d6 + 4` would require the player to roll a d6 and add four to the result.

The game or GM may also ask a player to make a check against a value, this is written as `xdn + y vs check`. For
example, given `1d8 + 2 vs 7`, the player has to roll a d8, add 2 to the result and see if the value is greater than
or equal to the value provided, the player succeeded the check, otherwise they failed. If a value lower than 1 would be
rolled, instead treat the result as a 1.

Of course, any of these values could depend on other parts of the game, such as character attribute. By convention, when
making a roll that includes an attribute of a character, use the attribute of the character performing the action. If a
check is requested, then use the attribute of the target.

For example, when attacking with `1d20 + STR vs AD`, the attacker will use their strength and add it to a 1d20 roll.
The defender uses their Armour Defence. If the attacker succeeds on the check, then they hit and roll their weapon's
damage die.

### Opposed Checks
Opposed checks can be called upon in the rules when multiple creatures are trying to compete with each other. Each
creature will perform a given roll, with the higher roll "winning". In the event of a tie, the creature with the highest
attribute modifier (if present) wins, with the GM ruling if this is a tie.

More than two creatures can perform opposed checks. The dice is only rolled once, and higher rolls "beat" lower rolls.

### Criticals
A critical happens in one of the following cases when rolling a d20 vs a check:

* The d20, before any modifiers, resulted in a 20.
* The combination of modifiers to both the roll and check result in a roll of 1 succeeding (that is, there is no way for
  the roll to fail).

Abilities may make reference to what happens on a critical.

## Bonus and Penalty Dice
When rolling a d20, certain game effects may require a player to roll a bonus or penalty dice. GMs may also chose to add
bonus or penalty dice if the situation requires it. Bonus or penalty dice can be one of d4, d6, d8 or d10, which the
player rolls in addition to the d20. If the dice is a bonus, then the result is added to the d20, if it is a penalty
then the result is subtracted instead, to a minimum of 1.

In case multiple bonus or penalty dice apply to the same roll, discard all but the highest of each. If both a bonus
and a penalty apply, take the difference (a d8 bonus and a d4 penalty would be a d4 bonus, for example) and round up to
a d4.
