---
layout: default
sidebarname: Survival
sidebartype: rules
sidebarsort: 2
---

# Survival Rules

## Supplies
"Supplies" are an abstract resource representing the food, water, shelter and other things that a hero out in the
wilderness needs to carry.

The amount of supplies a creature can carry and how much it needs to eat depends on its `bulk` attribute. A medium
creature has a bulk of 10, and a small creature has a bulk of 8.

A creature can carry up to `(10 + STR) * bulk` units of supplies supplies, and to get the full benefits of a long rest
they must consume `bulk` supplies. This means that a creature can carry enough supplies to last a number of days equal
to their strength bonus plus 10.

Creatures are, of course, able to share and trade supplies with other creatures.

## Environments
### Categories
Some abilities and the survival skill mention specific environments or allow a hero to chose an environment. The basic
list of environments are as follows:

* Wilderness (e.g. Forests, Swamps, Plains)
* Urban (e.g. Cities, Towns)
* Caverns
* Dungeons
* Arctic
* Desert
* Seas

<div class="optional">
The GM is, of course, allowed to add their own environment categories. But they should make any additions known to
players if their characters would realistically be aware of them.
</div>

### Threat Level
Individual locations have a threat level, which describes the availability of natural resources, and how difficult it is
to travel through them. This is determined by the GM based on the specifics of the environment or any conditions
affecting it.

#### Bountiful
The area is full of readily available food and water. Creatures in a bountiful environment are able to gather food and
water easily, and don't need to use supplies when resting.

#### Rich
A rich environment has resources available for those skilled enough to find it. A creature with a survival skill in
the environment's category does not need to use supplies when resting, but any other creatures must use the full amount
required.

#### Barren
A barren environment lacks resources, and heroes must prepare before venturing out into them. All creatures use the full
amount of supplies when resting, regardless of their survival skill.

#### Savage
A savage biome is hostile to those travelling through it, and requires specific preparations. As well as using the full
amount of supplies when resting, creatures must have supplies built for the specific environment.

For example, creatures venturing into a very hot desert would require "desert supplies" which include extra water and
specific enchanted clothing. The GM determines how difficult it is to get access to these specific supplies.

## Exhaustion
A creature that goes without food or sleep becomes exhausted.

* For every day a creature goes without sleeping, they get a point of exhaustion.
* For every day a creature goes without consuming at least half their required supplies, they get a point of exhaustion.

Note that this means that a creature can live off half their required supplies without penalty, although they do not get
the benefits of a long rest.

Each point of exhaustion reduces the creature's maximum HP by `Level + 1`. If a creature's max HP has been reduced below
0 due to this, then the creature perishes instantly. Exhaustion can be removed by taking a long rest (requiring supplies
and sleep), which removes two exhaustion points.

For example, a Level 2 fighter with 28HP goes without sleep, food and water for two days while doing training. They have
four points of exhaustion (two from lack of supplies and two from sleep). Their maximum HP is reduced by 12, becoming
16.
