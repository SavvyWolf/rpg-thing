---
layout: default
sidebarname: Skills
sidebartype: rules
sidebarsort: 3
---

# Skills

## Skills
Creatures are proficient in a number of skills equal to 4 plus their `INT` modifier. They represent what areas a
creature excels at. Whenever a creature attempts to do something that has a chance of failure, they must succeed at a
skill check.

In order to make a skill check, the GM selects an appropriate check value, ability score (`STR`, `DEX`, etc.) and skill.
The creature performing the action must roll a Standard Check (`1d20 + [stat] vs [check]`) in order to perform the
action as they intended. If they do not have proficiency in the required skill, they take a `d6` penalty dice.

The list of skills should be thorough enough such that most actions have a "close enough" skill that can be used, but
should that not be the case, the GM should decide whether or not the character is proficient with the action based on
their background.

Some skills indicate a specialization in brackets after their name (such as `Survival (Desert)`). They are considered
separate skills, and multiple can be chosen with different specialization.

Skills also provide access to [Tricks]({% link character/feats.md %}#tricks), which are feats which require experience
with a given skill.