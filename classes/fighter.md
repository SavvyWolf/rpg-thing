---
layout: default
sidebarname: Fighter
sidebartype: classes
---
# Fighter

{% include classtable.liquid class=site.data.classes.fighter %}

Fighter be a man (or woman) who hit things good.

## Initial Bonuses
{% include classbox.liquid class=site.data.classes.fighter %}

## leveling
{% include action.liquid action=site.data.actions.fighter_mindandbody %}
{% include action.liquid action=site.data.actions.fighter_multiattack %}
