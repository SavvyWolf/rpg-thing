---
layout: default
sidebarname: Wizard
sidebartype: classes
---
# Wizard

{% include classtable.liquid class=site.data.classes.wizard %}

Pew Pew!

## Initial Bonuses
{% include classbox.liquid class=site.data.classes.wizard %}

## leveling
{% include action.liquid action=site.data.actions.wizard_spellcasting %}
{% include action.liquid action=site.data.actions.readmagic %}
{% include action.liquid action=site.data.actions.wizard_spellbook %}
