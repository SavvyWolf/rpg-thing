---
layout: default
sidebarname: Skills
sidebartype: references
sidebarsort: 0
---

# Skills

This is a list of skills!

## Culture (Ancestry)
The Culture skill has an associated ancestry. Any creature with this skill is familiar with cultural and social norms of
that specific ancestry.

A creature proficient with this skill will automatically succeed in checks to:
* Read, write and speak the language associated with that ancestry.
* Interact with creatures of this ancestry without committing any faux pas.
* Recall any history, myths or legends which would be common knowledge to anyone of that ancestry.

A creature proficient with this skill will not take a penalty in checks to:
* Recall any history, myths or legends that is culturally significant to that ancestry.

Culture checks typically use `INT`, `WIS` or `CHA`.

<div class="optional">
This skill assumes that ancestries act as one homogeneous culture (all elves share a common culture, all
dwarves share a common culture, and so on). While this is true in the default setting for this system, as a GM you can
divide cultures differently to better suit your world.
</div>

## Survival (Environment)
The survival skill has an associated environment. Any creature with this skill is familiar with that environment, and
able to avoid danger and find food.

A creature proficient with this skill will automatically succeed in checks to:
* Avoid natural danger (such as dangerous plants or deep swamp water).
* Keep track of their location and navigate through the environment.

A creature proficient with this skill will not take a penalty in checks to:
* Identify unnatural changes in the environment (such as unusual plants, strange magical phenomena, unusually aggressive
wildlife, etc.).

In addition, creatures resting in a Rich environment which they have the appropriate Survival skill do not need to spend
supplies, as they are able to live off the land.

Survival checks typically use `WIS`.

## Crafting (Craft)
The crafting skill has a specific craft which the creature is skilled in. That creature is able to tools to make goods
that they can use or sell relating to that craft.

A creature proficient with this skill will automatically succeed in checks to:
* Identify and use the basic tools and materials used for their craft.
* Create simple products using their craft's basic tools.

A creature proficient with this skill will not take a penalty in checks to:
* Identify discernable properties of finished work for their craft. For example, a mason might notice a weak point in a
  stone arch.
* Make goods for the purpose of selling them.

The crafting type can be one of the following:
* TODO

Crafting checks typically use `INT`.