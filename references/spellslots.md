---
layout: default
sidebarname: Spell Slots
sidebartype: references
sidebarsort: 0
---

# Spell Slots

The amount of spell slots available depends on their class; they have access to one of the spell slot distributions
below. If a creature has access to multiple spell slot tables, they may only choose one, and do so after a long rest.

## Untrained Spellcasting
This is the table used by creatures that don't have any particular training or penchant for magic. While their class
doesn't give them access to spells, they may gain access to them through another ability.

Level 0 in this table represents a creature that does not have levels in a class.

| Level | 1st | 2nd | 3rd | 4th | 5th | 6th | 7th | 8th |
|-------|-----|-----|-----|-----|-----|-----|-----|-----|
| 0     | 1   | -   | -   | -   | -   | -   | -   | -   |
| 1     | 1   | -   | -   | -   | -   | -   | -   | -   |
| 2     | 1   | -   | -   | -   | -   | -   | -   | -   |
| 3     | 1   | -   | -   | -   | -   | -   | -   | -   |
| 4     | 2   | -   | -   | -   | -   | -   | -   | -   |
| 5     | 2   | -   | -   | -   | -   | -   | -   | -   |
| 6     | 2   | -   | -   | -   | -   | -   | -   | -   |
| 7     | 3   | -   | -   | -   | -   | -   | -   | -   |
| 8     | 3   | -   | -   | -   | -   | -   | -   | -   |
| 9     | 3   | -   | -   | -   | -   | -   | -   | -   |
| 10    | 3   | 1   | -   | -   | -   | -   | -   | -   |
| 11    | 3   | 1   | -   | -   | -   | -   | -   | -   |
| 12    | 3   | 1   | -   | -   | -   | -   | -   | -   |
| 13    | 3   | 1   | -   | -   | -   | -   | -   | -   |
| 14    | 3   | 2   | -   | -   | -   | -   | -   | -   |
| 15    | 3   | 2   | -   | -   | -   | -   | -   | -   |
| 15    | 3   | 2   | -   | -   | -   | -   | -   | -   |
| 16    | 3   | 2   | -   | -   | -   | -   | -   | -   |
| 17    | 3   | 3   | -   | -   | -   | -   | -   | -   |
| 18    | 3   | 3   | -   | -   | -   | -   | -   | -   |
| 19    | 3   | 3   | 1   | -   | -   | -   | -   | -   |
| 20    | 3   | 3   | 1   | -   | -   | -   | -   | -   |

## Wizard Spellcasting
This is the spell table used by Wizards.

| Level | 1st | 2nd | 3rd | 4th | 5th | 6th | 7th | 8th |
|-------|-----|-----|-----|-----|-----|-----|-----|-----|
| 1     | 2   | -   | -   | -   | -   | -   | -   | -   |
| 2     | 3   | -   | -   | -   | -   | -   | -   | -   |
| 3     | 4   | 2   | -   | -   | -   | -   | -   | -   |
| 4     | 4   | 3   | -   | -   | -   | -   | -   | -   |
| 5     | 4   | 4   | 2   | -   | -   | -   | -   | -   |
| 6     | 4   | 4   | 3   | -   | -   | -   | -   | -   |
| 7     | 4   | 4   | 3   | 2   | -   | -   | -   | -   |
| 8     | 4   | 4   | 3   | 3   | -   | -   | -   | -   |
| 9     | 4   | 4   | 3   | 3   | 1   | -   | -   | -   |
| 10    | 4   | 4   | 3   | 3   | 2   | -   | -   | -   |
| 11    | 4   | 4   | 3   | 3   | 2   | -   | -   | -   |
| 12    | 4   | 4   | 3   | 3   | 2   | 1   | -   | -   |
| 13    | 4   | 4   | 3   | 3   | 2   | 2   | -   | -   |
| 14    | 4   | 4   | 3   | 3   | 2   | 2   | -   | -   |
| 15    | 4   | 4   | 3   | 3   | 2   | 2   | 1   | -   |
| 15    | 4   | 4   | 3   | 3   | 2   | 2   | 1   | -   |
| 16    | 4   | 4   | 3   | 3   | 2   | 2   | 1   | -   |
| 17    | 4   | 4   | 3   | 3   | 2   | 2   | 1   | 1   |
| 18    | 4   | 4   | 3   | 3   | 2   | 2   | 1   | 1   |
| 19    | 4   | 4   | 3   | 3   | 2   | 2   | 1   | 1   |
| 20    | 4   | 4   | 3   | 3   | 2   | 2   | 1   | 1   |
