---
layout: default
sidebarname: Stages
sidebartype: character
sidebarsort: 0
---

# Creation

## Player Character
A player character is a hero who in some way has extraordinary abilities, either through training, natural talent,
decades of research or pledging themselves to a greater being (for better or worse).

As a player, you put yourself into their shoes, go on their adventures and decide how to overcome the many challenges
that stand in their way.

## Creating a Character
First, skim through the list of classes and ancestries and think about what type of character you want to make. Come up
with a rough concept, and see what suits them best. Consider their skillset and where their strengths and weaknesses
lie.

The stages to go through to create a character are as follows:

* Select an ancestry
* Select a background
* Select a class

### Ancestry
Select an available ancestry. Many ancestries will have multiple variants for you to chose from. You get all abilities
and proficiencies from your chosen ancestry, as well as its initial ability bonus, language and HP.

Note down your initial ability bonus, size, speed, HP, languages and any given abilities or bonuses.

<div class="optional">
Normally your base ability bonuses are determined by your ancestry, however a GM may wish to allow players to choose them
freely. If this is the case, they can allow players to assign the following values to bonuses as they wish: -1, -1, +0,
+0, +1, +1.
</div>

### Background
Select a background from the backgrounds list. This will give you a boost to two ability bonuses, and some initial skill
proficiencies.

Add these bonuses to your ability bonuses, and make a note of skill proficiencies or any other benefits.

### Class
Players gain the first level of a class they select at the start of the game. Choose a class, and you gain the initial
and first level benefits from that class.

You gain the following:
* The class's boosts to ability bonuses.
* Initial weapon and armour proficiencies.
* The initial HP (you get this on top of HP from your ancestry).
* A number of disciplines you have access to.

Then select techniques, tricks and spells according to the number given in the appropriate column in the class table for
level 1. You may only pick techniques and spells with a discipline you have access to, and you may only pick tricks for
skills you are proficient in.

If your class gives you additional proficiencies or disciplines by actions at level 1, you can use them as prerequisites
for techniques, tricks and spells at level 1.

You also gain the "Initial Ability Boost" and "Initial Skill Proficiencies" boon:
{% include action.liquid action=site.data.actions.initialboost %}
{% include action.liquid action=site.data.actions.initialproficiencies %}

## Leveling Up
To level up a character, consult the appropriate row in your ancestry and class tables, and gain any additional
techniques, tricks, spells and feats.

You also gain the Ability Boost and HP Boost boons:
{% include action.liquid action=site.data.actions.abilityboost %}
{% include action.liquid action=site.data.actions.hpboost %}
