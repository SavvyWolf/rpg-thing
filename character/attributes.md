---
layout: default
sidebarname: Attributes
sidebartype: character
sidebarsort: 1
---

# Attributes

## Ability Scores
A creature has 6 base ability bonuses, which determine many of their other attributes. The bonuses are as follows:

| Stat | Name         | Description                                  |
|------|--------------|----------------------------------------------|
| STR  | Strength     | Physical strength and power                  |
| DEX  | Dexterity    | Physical nimbleness and agility              |
| CON  | Constitution | Physical resilience and endurance            |
| INT  | Intelligence | Mental problem solving and learned knowledge |
| WIS  | Wisdom       | Mental intuition and awareness               |
| CHA  | Charisma     | Mental confidence and people skills          |

A bonus of `0` is considered average across all creatures. As adventurers continue to improve and grow stronger, their
bonuses will increase.

## Build
A creature's build is mostly determined by its size. Physically larger creatures are easier to hit and shrug off poisons
easier, however smaller creatures find it easier to dodge and land attacks.

Most playable ancestries have `medium` and `small` builds.

|       | Fine | Diminutive | Tiny | Small |
|-------|------|------------|------|-------|
| BLD   | +12  | +8         | +4   | +0    |
| Space | 1ft  | 2.5ft      | 5ft  | 5ft   |
| Reach | 1ft  | 2.5ft      | 5ft  | 5ft   |
| Bulk  | 1    | 2          | 4    | 8     |

|       | Medium | Large | Huge | Colossal |
|-------|--------|-------|------|----------|
| BLD   | -0     | -4    | -8   | -12      |
| Space | 5ft    | 10ft  | 15ft | 20ft     |
| Reach | 5ft    | 10ft  | 15ft | 15ft     |
| Bulk  | 10     | 15    | 20   | 30       |

A creature's `BLD` is a modifier used in some rolls for abilities, notably normal attack rolls. It is also included in
a creature's Armour and Reflex checks.

A creature's `space` is the amount of space it controls on the battlefield. This is not to say a medium creature is a 5ft
cube of meat, but such a creature would have difficulty passing another creature in a 5ft wide corridor.

When playing on a grid, a creature's `space` is how many grid squares a creature should take up. If the grid squares are
bigger than a creature, then it takes up an entire square, but multiple creatures of its size or smaller can reside in
that square.

The `reach` value is the default range of a weapon (or unarmed attack) wielded by that creature. This is determined by
the weapon itself, but is provided here since most creatures will use weapons designed for their build.

A creature's `bulk` determines how much it can carry (based on its strength) and how much food it needs to consume per
day. See the appropriate sections for more detail.

Larger creatures tend to also have a higher `CON` score than smaller ones, but this is included in the creature's
stat block. See (TODO) for details on creature size when creating creatures.

### Pebbles
In the world of (TODO), merchants wishing to transport goods have standardised on a unit of measurement known as
"pebbles" (based on stones of a specific shape and weight which can be found in guild and trade halls). How many
"pebbles" an item is worth is determined mostly by it's weight, although particularly bulky or dangerous items cost more
pebbles.

### Carrying Capacity
A creature can carry up to `(10 + STR) * bulk` pebbles worth of equipment on their person at a time without penalty. If
they are carrying between that and `(10 + STR) * bulk * 2` pebbles worth of equipment, their movement speed is halved
and they take a d8 penalty die to all `DEX` rolls. A creature carrying more than that is immobile.

Note that this does not include supplies, which have a separate carrying capacity.

A creature can carry (or be mounted by) a creature at most one size category smaller than it without penalty. For
gameplay purposes, a creature weighs `(10 + STR) * bulk` pebbles.

## Defences
A creature has 5 "defence" scores, which function as their ability to avoid and mitigate hostile effects. A roll is made
against a specific defence. If the final result of the roll is higher or equal to the defence value, then the check
succeeds.

A creature may, if it wishes, intentionally fail a check.

### Armour Defence
`5 + Armour Bonus + DEX + BLD`

This check is used for effects that can either be dodged, or absorbed/deflected by armour. Typically, this includes
weapon effects and some magical effects.

Wearing certain armour may limit the amount of DEX that can contribute to a creature's reflex check.

### Reflex Defence
`5 + DEX + BLD`

This check is used for effects that can be dodged and ignore armour. This typically includes magical beams, which only
need to make contact to have an effect.

Wearing certain armour may limit the amount of DEX that can contribute to a creature's reflex check.

### Fortitude Defence
`5 + CON`

This check is used for poisons, diseases and other such things that attack a creature's body directly. These are
typically things that the creature's body needs to "fight off".

### Endurance Defence
`5 + STR`

This check is used for checks which require their target to "stand their ground". Such as being shoved or grappled.

### Will Defence
`5 + WIS`

This check is used for mind altering effects, and represents a creatures ability to maintain its composure. This is
typically from magical effects that frighten, confuse or dominate creatures.
