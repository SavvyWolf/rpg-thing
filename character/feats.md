---
layout: default
sidebarname: Feats
sidebartype: character
sidebarsort: 2
---

# Feats
Creatures have many unique and powerful abilities. To represent this, creatures have a number of feats which provide
a one time effect, a passive boost or an action that can be performed in combat.

Feats which provide a one time effect ("boons") don't need to be noted down on a character sheet after their effect has
been completed. Passive effects are assumed to be always "on", although unless otherwise noted, they may be suppressed
by the creature.

Many feats are actions that can be taken in or out of combat. Any feat that isn't a passive or boon is an "action".

With the exception of common actions and actions with an untrained penalty, creatures can only use feats they have
learned. Typically, for player characters, feats are gained from leveling up, while monsters have all their known
actions in their stat blocks.

Each feat may also have prerequisites (that must be met in order to use an action) and several tags that other
abilities or conditions can refer to.

Feats may be one of the following types, which have rules as described in the following sections:
* Common
* Abilities
* Tricks
* Techniques
* Spells

## Free vs Chosen Feats
Classes allow characters to chose a certain number of Tricks, Techniques and Spells, typically based on the value of a
bonus. Feats chosen these way are referred to as "Chosen Feats", while feats from other sources (such as a background,
class or ancestry) are referred to as "Free Feats".

Free Feats do not count against the limit for Chosen Feats. Any temporary bonus or penalty to ability scores does not
cause the creature to learn or forget any feats.

## Common
Common feats are actions that all creatures, barring exceptional circumstances, can perform. Unless specified otherwise,
creatures are proficient in all common feats. These include simple things like moving around in combat or striking
someone with a weapon.

## Abilities
An ability is an innate ability a character has access to, either through their ancestry or perhaps through a divine
blessing. These actions would be things like low light vision for Dwarves.

Abilities typically become unavailable when shapeshifted, but are usually not stopped by actions that stop or inhibit
magic.

## Tricks
Tricks (as in, "tricks of the trade") are specific expressions of skills that grant specific actions to creatures.

Many skills give a selection of tricks for free, and certain backgrounds and other features may give access to
additional tricks. Classes give characters access to a number of Chosen Tricks. Any trick chosen here must be for a
skill the creature is proficient in.

Tricks can typically be used freely and as often as the creature likes, as long as conditions allow.

## Techniques
Techniques are actions that a character learns through training and experience, and usually come from their class to
allow them to do class specific abilities. Physically oriented characters tend to know a lot of techniques which
determine how they use their skills in battle.

Classes give characters access to a number of Chosen Techniques. A Technique chosen in this way must be of a discipline
that the creature is proficient in.

Some techniques can also be used untrained, but with a penalty dice.

Techniques can have cooldowns; once used they cannot be used for a specific number of rounds depending on the action.
This is because certain openings may only be available for a few seconds, and creatures in battle will be aware of any
previous attempts.

## Spells
Spells are magical effects that certain heroes can create that bend the rules of reality. Heroes that can cast spells
have access to a specific number of spell slots for each rank from 1 to 9. Spells themselves have a minimum rank they
can be cast at.

Some Classes give characters access to a number of Chosen Spells. A Spell chosen in this way must be of a discipline
that the creature is proficient in.

A hero can expend a spell slot of the spell's level or higher to perform the action.