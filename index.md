---
layout: default
---

## Title
Welcome to (name tbd) RPG!

## Game Rules
{% include sidebarlist.liquid type="rules" %}

## Character Creation
{% include sidebarlist.liquid type="character" %}

## References
{% include sidebarlist.liquid type="references" %}

## Classes
{% include sidebarlist.liquid type="classes" %}

## Ancestries
{% include sidebarlist.liquid type="ancestries" %}

## Feats
{% include sidebarlist.liquid type="actions" %}
